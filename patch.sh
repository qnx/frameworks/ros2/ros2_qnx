# Function for apply a patch
# $1 arg is the directory
# $2 arg is the patch name
qnx_patch () {
    cd $1
    git apply ${root_dir}/qnx_patches/$2
    cd -
}

root_dir=${PWD}

# Apply QNX patches
qnx_patch ./src/ros2/rcutils rcutils.patch
qnx_patch ./src/ros2/rosbag2 rosbag2.patch
